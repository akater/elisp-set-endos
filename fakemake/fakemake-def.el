;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'set-endos
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("set-endos-state-based"
                       "set-endos")
  site-lisp-config-prefix "50"
  license "GPL-3")

(defvar root)
(defvar etcdir)
(advice-add #'fakemake-install :after
            (lambda ()
              (with-filenames-relative-to root (etcdir)
                (copy-file "endomorphism.png" etcdir t))))
